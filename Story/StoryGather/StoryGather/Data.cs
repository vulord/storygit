﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoryGather
{
    public class OneStory
    {
        public String Title = "";
        public String Introduction = "";
        public String Review = "";
        public String Conclusion = "";
        public List<Post> Chapter = new List<Post>();
        public DateTime DateCreate;
        public String FontTitle = "";

    }

    public class Post
    {
        public String Content = "";
        public List<int> LinkPosition = new List<int>();
        public List<String> Link = new List<string>();
        public int PostNumber = 0;
    }

}
