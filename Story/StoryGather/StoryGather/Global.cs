﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoryGather
{
    public class Global
    {
        /*  Global variables    */
        public static String Link;
        public static OneStory MyStory;

        public static Form1 One;
        public static Form2 Two;
        

        public static Form1 GetFormOne()
        {
            if (One == null)
                One = new Form1();
            return One;
        }
        public static Form2 GetFormTwo()
        {
            if (Two == null)
                Two = new Form2();
            return Two;
        }
        
        /*  variables for class */
        public static Functions SupFunction = new Functions();
    }
}
