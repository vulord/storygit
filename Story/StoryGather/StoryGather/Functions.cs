﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Windows.Forms;

namespace StoryGather
{
    public class Functions
    {
        public string GetWebString()
        {
            string appURL = Global.Link;
            HttpWebRequest wrWebRequest = WebRequest.Create(appURL) as HttpWebRequest;
            HttpWebResponse hwrWebResponse = (HttpWebResponse)wrWebRequest.GetResponse();

            StreamReader srResponseReader = new StreamReader(hwrWebResponse.GetResponseStream());
            string strResponseData = srResponseReader.ReadToEnd();
            srResponseReader.Close();
            return strResponseData;
        }

        public void DownloadFile(string url, string fullname)
        {
            using (WebClient webClient = new WebClient())
            {
                webClient.DownloadFile(url, fullname);
            }
        }

        //type 1: day-month-year
        //type 2: month-day-year
        public DateTime ParseDateTime(string x, int type)
        {
            int d, m, y;
            d = 1;
            m = 1;
            y = 1;
            int pos = 0;
            List<char> tmp = new List<char>();
            while (pos < x.Length && x[pos] != '-' && x[pos] != '/')
            {
                tmp.Add(x[pos]);
                pos++;
            }
            pos++;
            if (type == 1)
            {
                d = Convert.ToInt32(new string(tmp.ToArray()));
            }
            else
            {
                m = Convert.ToInt32(new string(tmp.ToArray()));
            }
            tmp.Clear();
            while (pos < x.Length && x[pos] != '-' && x[pos] != '/')
            {
                tmp.Add(x[pos]);
                pos++;
            }
            pos++;
            if (type == 1)
            {
                m = Convert.ToInt32(new string(tmp.ToArray()));
            }
            else
            {
                d = Convert.ToInt32(new string(tmp.ToArray()));
            }
            tmp.Clear();
            while (pos < x.Length)
            {
                tmp.Add(x[pos]);
                pos++;
            }
            pos++;
            y = Convert.ToInt32(new string(tmp.ToArray()));
            tmp.Clear();

            return new DateTime(y, m, d);
        }


        public void LoadWebPage()
        {
            String x = GetWebString();
            StreamWriter writer = new StreamWriter(Directory.GetCurrentDirectory() + "\\tmp.html");
            writer.WriteLine(x);
            writer.Close();
            Global.MyStory = new OneStory();
            //get title
            int pos = x.IndexOf("threadtitle");
            pos = pos + 14;
            for (; pos < x.Length && x[pos] != '>'; pos++)
            {
            }
            pos++;
            List<char> tmp = new List<char>();
            while ((pos + 1) < x.Length && x[pos] != '<' && x[pos + 1] != '/')
            {
                tmp.Add(x[pos]);
                pos++;
            }
            Global.MyStory.Title = new string(tmp.ToArray());
            tmp.Clear();
            int position = 0;
            //read each post, get content, write to a new file, search for introduction
            bool check = true;
            while (check == true)
            {
                pos = x.IndexOf("postbit postbitim postcontainer old", pos);
                int tmppost = pos + 37;
                int tmpdate = x.IndexOf("span class=\"date\">", tmppost);
                tmpdate = tmpdate + 18;
                while (x[tmpdate] != ',')
                {
                    tmp.Add(x[tmpdate]);
                    tmpdate++;
                }
                //this date also use for first chapter
                String Dateneed = new String(tmp.ToArray());
                tmp.Clear();
                if (position == 0)
                    Global.MyStory.DateCreate = Global.SupFunction.ParseDateTime(Dateneed, 1);
                //get post count
                pos = x.IndexOf("class=\"postcounter\">", pos);
                pos = pos + 21;
                while (x[pos] != '<')
                {
                    tmp.Add(x[pos]);
                    pos++;
                }
                position = Convert.ToInt32(new string(tmp.ToArray()));
                tmp.Clear();
                //get username
                pos = x.IndexOf("div class=\"username_container\">", pos);
                pos = x.IndexOf("<strong>", pos);
                pos = pos + 8;
                while (x[pos] != '<')
                {
                    tmp.Add(x[pos]);
                    pos++;
                }
                String author = new String(tmp.ToArray());
                tmp.Clear();
                // get content
                pos = x.IndexOf("<blockquote class=\"postcontent restore\">", pos);
                pos = pos + 40;
                tmppost = x.IndexOf("</blockquote>", pos);
                while (pos != tmppost)
                {
                    //add to tmp
                    tmp.Add(x[pos]);
                    pos++;
                }
                string content = new string(tmp.ToArray());
                tmp.Clear();
                
                //analyze content
                for (int i = 0; i < content.Length; i++)
                {
                    if (content[i] == '<')
                    {
                        //check if there is a html syntax or not

                    }
                }
            }

        }

    }
}
